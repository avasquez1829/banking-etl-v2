import os, datetime
import pandas
import json
import shutil
import datetime
from flask import current_app as app
from unicodedata import normalize
import unicodedata
from fuzzywuzzy import fuzz
import psycopg2


class DataScrap:

    routes = {
        'root': app.root_path,
        'scripts': app.root_path + '/scripts/',
        'folder': app.root_path + '/files/',
        'sql': app.root_path + '/sql/',
        'dictionary': app.root_path + '/dictionary/'
    }

    

    months = {
        'Enero': '01',
        'Febrero': '02',
        'Marzo': '03',
        'Abril': '04',
        'Mayo' : '05',
        'Junio': '06',
        'Julio': '07',
        'Agosto': '08',
        'Septiembre': '09',
        'Octubre': '10',
        'Noviembre': '11',
        'Diciembre': '12'
    }

    def get_files_in_folder(self):
        return os.listdir(self.routes['folder'])
    
    #inicio metodos de carga a postgres
    def scripts_db (self):
        return os.listdir(self.routes['sql'])

    def insert_balance(self,sql_script):
        DB_NAME="test_db"
        DB_USER="postgres"
        DB_PASS="admin2020"
        DB_HOST="localhost"
        DB_PORT="5432"

        try:
            conn= psycopg2.connect(database= DB_NAME,user=DB_USER,password=DB_PASS,host=DB_HOST,port=DB_PORT)
            print("conexión exitosa")
            cur= conn.cursor()
            cur.execute(sql_script)
            conn.commit()
            #print(sql_script)
        except:
            print("no hay conexión")
    #fin metodos de carga a postgres

    def search_account(self, data, account):
        obj = None
        for a in data:
            ratio = fuzz.ratio(a['name'].upper(), account.upper())
            if ratio > 93:
                obj = a
        return obj

    def create_insert_script(self, file):

        #acdf = pandas.read_excel(self.routes['dictionary'] + 'accountcatalog.xlsx')

        balance_catalog = pandas.read_excel(self.routes['dictionary'] + 'catalagobalance.xlsx')
        estado_catalog = pandas.read_excel(self.routes['dictionary'] + 'catalagoestado.xlsx')

        bkdf = pandas.read_excel(self.routes['dictionary'] + 'banks.xlsx')

        bkdf.columns = ['id', 'banks']

        balance_catalog.columns = ['id', 'description']
        estado_catalog.columns = ['id', 'description']

        df = pandas.read_excel(self.routes['folder'] + file)
        tipo = (file[3:file.find('-BANCO-en-')])

        if tipo == "ESTADO":

            dictionary_estado = ['Ingresos Por Intereses',
                                 'Egresos de Operaciones',
                                 'Ingreso Neto de Intereses',
                                 'Otros Ingresos',
                                 'Ingresos de Operaciones',
                                 'Egresos Generales',
                                 'Utilidad antes de Provisiones',
                                 'Utilidad del Periodo']

            cols = [1, 2, 3, 16]
            year = str(df.iloc[7, 4])
            df.drop(df.columns[cols], axis=1, inplace=True)
            bank = df.iloc[1, 0]

            df[8:9] = df[8:9].replace('Enero', self.months['Enero'])
            df[8:9] = df[8:9].replace('Febrero', self.months['Febrero'])
            df[8:9] = df[8:9].replace('Marzo', self.months['Marzo'])
            df[8:9] = df[8:9].replace('Abril', self.months['Abril'])
            df[8:9] = df[8:9].replace('Mayo', self.months['Mayo'])
            df[8:9] = df[8:9].replace('Junio', self.months['Junio'])
            df[8:9] = df[8:9].replace('Julio', self.months['Julio'])
            df[8:9] = df[8:9].replace('Agosto', self.months['Agosto'])
            df[8:9] = df[8:9].replace('Septiembre', self.months['Septiembre'])
            df[8:9] = df[8:9].replace('Octubre', self.months['Octubre'])
            df[8:9] = df[8:9].replace('Noviembre', self.months['Noviembre'])
            df[8:9] = df[8:9].replace('Diciembre', self.months['Diciembre'])
        
            df[8:9] = (str(year) + '-' + df[8:9] + '-01')

            df = df.drop([0, 1, 2, 3, 4, 5, 6, 7])

            df.iloc[0, 0] = 'Cuentas'

            trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)

            if df.iloc[0, 0] == 'Cuentas':
                i = 1
                account_id = None
                bank_id = bkdf.loc[bkdf['banks'] == bank.upper()]
                balance_date = None
                out = ''
                temp_bank = None
                temp_ratio = 100
                temp_acc = None
                acum = 0

                for item in bkdf['banks']:
                    ratio = fuzz.ratio(item.upper(), bank.upper())
                    new_ratio = 100 - ratio

                    if new_ratio >= 0 and new_ratio <= 30:
                        if new_ratio < temp_ratio:
                            temp_ratio = new_ratio
                            bank_id = bkdf.loc[bkdf['banks'] == item]
                            for id in bank_id['id']:
                                temp_bank = int(id)

                if temp_bank == None:
                    print("No existe")

                for account in df.iloc[1:, 0]:
                    date = 1
                    temp_ratio_2 = 100
                    
                    for a in dictionary_estado:
                        if account == a:
                            if account == dictionary_estado[3] and acum == 0:
                                temp_acc = dictionary_estado[0]
                                acum = acum + 1
                            elif account == dictionary_estado[3] and acum == 1:
                                temp_acc = dictionary_estado[3]
                                account = ''
                                acum = acum + 1
                            elif account == dictionary_estado[3] and acum == 2:
                                temp_acc = dictionary_estado[3]
                                account = temp_acc + ' '
                                acum = acum + 1
                            else:
                                temp_acc = account

                    if temp_acc != None and temp_acc != account:
                        account = temp_acc + ' ' + account
                    
                    account = normalize('NFKC', normalize('NFKD', account).translate(trans_tab))

                    for item in estado_catalog['description']:
                        ratio = fuzz.ratio(item.upper(), account.upper())
                        new_ratio_2 = 100 - ratio
                        if new_ratio_2 >= 0 and new_ratio_2 <= 30:
                            if new_ratio_2 < temp_ratio_2:
                                temp_ratio_2 = new_ratio_2
                                acc_id = estado_catalog.loc[estado_catalog['description'] == item]
                                for id in acc_id['id']:
                                    account_id = int(id)

                    for amount in df.iloc[i, 1:]:
                        balance_date = df.iloc[0, date]
                        out += "INSERT INTO public.banks_accountbalance (id, account_id, bank_id, balance_date, balance_amount) VALUES ((SELECT (max(id) + 1) FROM banks_accountbalance), {}, {}, '{}', {}); \n".format(account_id, temp_bank, balance_date, float(amount * 1000) )
                        date += 1

                    i = i + 1

                return out
                    

        if tipo == "BALANCE":

            balance_data = json.load(open(self.routes['dictionary'] + 'balance_dictionary.json',encoding='UTF-8'))
            dictionary_balance = []
            
            for i in balance_data:
                dictionary_balance.append(i['name'])

            cols = [3, 4, 5]
            df.drop(df.columns[cols], axis=1, inplace=True)
            year = str(df.iloc[7, 3])

            bank = df.iloc[1, 0]

            cols = [1, 2]
            df.drop(df.columns[cols], axis=1, inplace=True)

            df[8:9] = df[8:9].replace('Enero', self.months['Enero'])
            df[8:9] = df[8:9].replace('Febrero', self.months['Febrero'])
            df[8:9] = df[8:9].replace('Marzo', self.months['Marzo'])
            df[8:9] = df[8:9].replace('Abril', self.months['Abril'])
            df[8:9] = df[8:9].replace('Mayo', self.months['Mayo'])
            df[8:9] = df[8:9].replace('Junio', self.months['Junio'])
            df[8:9] = df[8:9].replace('Julio', self.months['Julio'])
            df[8:9] = df[8:9].replace('Agosto', self.months['Agosto'])
            df[8:9] = df[8:9].replace('Septiembre', self.months['Septiembre'])
            df[8:9] = df[8:9].replace('Octubre', self.months['Octubre'])
            df[8:9] = df[8:9].replace('Noviembre', self.months['Noviembre'])
            df[8:9] = df[8:9].replace('Diciembre', self.months['Diciembre'])
        
            df[8:9] = (str(year) + '-' + df[8:9] + '-01')


            df = df.drop([0, 1, 2, 3, 4, 5, 6, 7])


            df.iloc[0, 0] = 'Cuentas'

            trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)

            if df.iloc[0, 0] == 'Cuentas':
                i = 1
                account_id = None
                bank_id = None
                balance_date = None
                out = ''
                temp_bank = None
                temp_ratio = 100
                header = None
                data = []
                
                for item in bkdf['banks']:
                    ratio = fuzz.ratio(item.upper(), bank.upper())
                    new_ratio = 100 - ratio
                    
                    if new_ratio >= 0 and new_ratio <= 30:
                        if new_ratio < temp_ratio:
                            temp_ratio = new_ratio
                            bank_id = bkdf.loc[bkdf['banks'] == item]
                            for id in bank_id['id']:
                                temp_bank = int(id)

                if temp_bank == None:
                    print("No existe")

                
                data.append(balance_data)

                for account in df.iloc[1:, 0]:
                    obj = None
                    date = 1
                    account = normalize('NFKC', normalize('NFKD', account).translate(trans_tab))


                    while obj == None:
                        obj = self.search_account(data[-1], account)

                        if obj:
                            account_id = obj['id']
                            if 'sub_accounts' in obj:
                                data.append(obj['sub_accounts'])
                            break
                        else:
                            if len(data) > 1:
                                data.pop()

                        
                    for amount in df.iloc[i, 1:]:
                        balance_date = df.iloc[0, date]
                        out += "INSERT INTO public.banks_accountbalance (id, account_id, bank_id, balance_date, balance_amount) VALUES ((SELECT (max(id) + 1) FROM banks_accountbalance), {}, {}, '{}', {}); \n".format(account_id, temp_bank, balance_date, float(amount * 1000) )
                        date += 1

                    i = i + 1

                return out
                    