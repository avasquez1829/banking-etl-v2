import os,datetime
import shutil
import time
import traceback
import pandas
import scrapping_utils
from classes import DataScrap


def transform_and_load_data():
    #====================== Rutas a utilizar ======================#
    print("Inicia el proceso de ETL")
    # para = cl.parameters('2019', 'November')
    cl = DataScrap()
    files = cl.get_files_in_folder()
    sqlscript = ''
    #-----------------------------limpia carpeta antes de procesar xlsx----------------------------------
    file_sql = cl.scripts_db()
    for file in file_sql:      
        if file.endswith(".sql"):
            path_sql = os.path.join(os.getcwd(), "sql_data",file)
            os.remove(path_sql)
    #----------------------------------------------------------------------------------------------------
    path_source= os.path.join(os.getcwd(), "DOWNLOADED_FILES","procesados")
    year =str(datetime.datetime.now().year)
    mes_actual = datetime.datetime.now()
    mes_actual = mes_actual.strftime("%B")
    #Quita el directorio de procesados  si existe
    path_remove= os.path.join(os.getcwd(), "DOWNLOADED_FILES","procesados",year,mes_actual)
    if os.path.exists(path_remove):
       shutil.rmtree(path_remove)  
    for file in files:

        if file.endswith(".xlsx"):
            print("=== Inicia el ciclo para el archivo ===" + file)
            
            try:
                #fecha_creacion = time.strftime('%Y%m%d')
                #fecha_subida = time.strftime('%Y%m%d')
                #bank_short_name = (file[file.find('-en-')+4:-5])
                #tipo = (file[3:file.find('-BANCO-en-')])
                sqlscript += cl.create_insert_script(file)
                #inicio moviendo archivos a procesados                
                path_xlsx = os.path.join(os.getcwd(), "DOWNLOADED_FILES",year,mes_actual,file)
                if not os.path.exists(path_source + '\\'  + year + '\\' + mes_actual):
                   os.makedirs(path_source + '\\' + year + '\\' + mes_actual)
                path_month = ''.join([path_source,'\\', year, '\\', mes_actual, '\\'])
                shutil.move(path_xlsx,path_month)
                #fin moviendo archivos a procesados
            except ValueError as e:
                print("************************* ERROR *****************************" + str(e))

    script_name = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    with open('sql_data/{}.sql'.format(script_name), 'w') as f:
       f.write(sqlscript)
    #---------------------------------------------------------------
    
    #------------Inicio Carga de sql--------------------
    file_sql_insert = cl.scripts_db()
    for file in file_sql_insert:
        if file.endswith(".sql"):
            path_sql = os.path.join(os.getcwd(), "sql_data",file)
            path_mov = os.path.join(os.getcwd(), "sql_data","procesado")
            sql_file = open(path_sql)
            sql_as_string = sql_file.read()
            cl.insert_balance(sql_as_string,year)
            sql_file.close()
            shutil.move(path_sql,path_mov)
    #----------Fin Carga de sql---------------------------- 
