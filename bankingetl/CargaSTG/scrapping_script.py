from datetime import datetime
import os
import shutil
import time
import requests
from selenium import webdriver
from selenium.common.exceptions import ElementNotVisibleException
import scrapping_utils, carga_stg
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def scrapping_page(type, year, month):
    scraping_object = scrapping_utils.ScrappingUtils()
    scraping_object.generator_paths(year, month)
    c = 1

    url = "".join([scraping_object.url, str(year)])
    download_path = scraping_object.down_path

    options = webdriver.FirefoxProfile()

    # evita que se use directorio por defecto
    options.set_preference("browser.download.folderList", 2)

    # Evita notificaciones de descarga
    options.set_preference("browser.download.manager.showWhenStarting", False)

    # Configura el nuevo directorio de descarga
    options.set_preference("browser.download.dir", download_path)

    # Descarga automaticamente archivo xlsx sin confirmar descarga
    options.set_preference(
        "browser.helperApps.neverAsk.saveToDisk",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )

    driver = webdriver.Firefox(
        firefox_profile=options,
        executable_path=os.path.join(scraping_object.root_path, "geckodriver")
    )
    driver.get(url)
    try:
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='ui-dialog-buttonset']/button"))
        )
        element.click()
    except ElementNotVisibleException:
        driver.quit()

    files = driver.find_elements_by_xpath(
        f"//div[@class='field-content']//a[contains(@href,'RE-{type}-BANCO')  and contains(@href,'xlsx')]"
    )
    for element in files:
        check_url_status = requests.get(element.get_attribute('href'))
        if check_url_status.status_code == 200:
            element.click()
            time.sleep(2)
            paths = [
                os.path.join(download_path, fname) for fname in
                os.listdir(download_path)
            ]
            while c != 0:
                if max(paths, key=os.path.getctime).endswith(
                        ".crdownload"
                ):
                    c = c + 1
                    time.sleep(1)
                else:
                    c = 0
            files = max(paths, key=os.path.getctime)
            filename = files[len(download_path):]
            if not filename.endswith(".xlsx"):
                print("El archivo " + filename + " ha fallado")
                continue
            try:
                scrapping_utils.move_file_super(download_path, filename)
            except ():
                pass
            time.sleep(3)
    time.sleep(4)
    driver.close()


def start_scrapping():
    year = str(datetime.now().year)
    month = datetime.now().strftime("%B")
    report_types = ["ESTADO", "BALANCE"]
    for report_type in report_types:
        scrapping_page(report_type, year, month)
    carga_stg.transform_and_load_data()


start_scrapping()
