--Temporal con data ajustada
DROP TABLE IF EXISTS account_temp;

CREATE TEMP TABLE account_temp AS(
select distinct b.id, b.seq_id,c.id bank,a.fecha, a.amount   from (
select  (substring(period,1,4)|| case when month<10 then '0' || cast(month as varchar) else cast(month as varchar) end || '01')as fecha, row_number ()over (partition by period order by id) as row, amount, creation_date,
upload_date, tipo,bank, short_name
from stg_cuentasinicial 
) a join  accountcatalog b on a.row=b.seq_id and lower(a.tipo)=lower(b.doc_type)
join banks c on (lower(a.bank)=lower(c.bank_name) or a.short_name =c.short_name )	
order by fecha,seq_id);
--Se inserta en la tabla de auditoria toda cuenta que haya sufrido cambios
Insert into stg_account_balance_audit
select b.id,cast(a.id as int), b.balance_date, b.balance_amount, a.amount from account_temp a join accountbalance b  
on a.id=b.account_id
and cast(bank as int) =b.bank_id and cast(a.fecha as date)= b.balance_date 
where Round(a.amount,2)<>b.balance_amount;

--Se inserta la data de cuentas nuevas.
 INSERT INTO accountbalance (account_id, bank_id, balance_date, balance_amount)
select  a.id, bank, cast(fecha as date), amount  from account_temp a left join accountbalance b
ON  a.id=b.account_id and a.bank=b.bank_id and cast(a.fecha as date)= b.balance_date where b.account_id is null
and not cast(fecha as date)>cast((now() - interval '3 month') as date);

--Actualizamos todas las cuentas que sufrieron cambios
 UPDATE accountbalance a
set balance_amount=b.amount
from account_temp b where a.account_id=b.id and a.balance_date=cast(b.fecha as date) and a.balance_amount<>b.amount 
and a.bank_id=b.bank; 




	