\set VERBOSITY verbose
TRUNCATE TABLE stg_cuentaInicial;
TRUNCATE TABLE stg_cuentasInicial;

\set VERBOSITY verbose
\COPY stg_cuentaInicial FROM '/home/gbitcorp/Escritorio/python-etl-scrapy/bankingetl/CargaSTG/Archivos/Temporal/Banking_TMP_STG.csv' DELIMITERS '|' CSV HEADER;

insert into stg_cuentasinicial(id, account, period, amount, creation_date, upload_date, tipo, bank,short_name, month)
select  distinct a.*, b.id as month   from stg_cuentainicial a join stg_months b on substring(period,6,length(period))= month_spa;
