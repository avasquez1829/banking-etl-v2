import os
import pandas
import shutil
import datetime


class ScrappingUtils:
    def __init__(self):
        self.root_path = ''
        self.path_script = ''
        self.path_procesados = ''
        self.path_rechazados = ''
        self.path = ''
        self.path_temp = ''
        self.path_log = ''
        self.path_sql = ''
        self.path_bat = ''
        self.filestg = ''
        self.url = "https://www.superbancos.gob.pa/es/fin-y-est/reportes-estadisticos?field_ano_rep_est_value="
        self.down_path = ''
        self.filelist = None

    def generator_paths(self, year, month):

        self.root_path = os.getcwd()
        self.path_script = os.path.join(self.root_path, "Scripts")
        self.path_procesados = os.path.join(self.root_path,
                                            'Archivos',
                                            'Procesados',
                                            year,
                                            month)

        if not os.path.exists(self.path_procesados):
            os.makedirs(self.path_procesados)

        self.path_rechazados = os.path.join(self.root_path,
                                            'Archivos',
                                            'Rechazados',
                                            year,
                                            month)

        if not os.path.exists(self.path_rechazados):
            os.makedirs(self.path_rechazados)

        self.path = os.path.join(self.root_path,
                                 'DOWNLOADED_FILES',
                                 year,
                                 month)
        self.path_temp = os.path.join(self.root_path, 'Archivos', 'Temporal\\')
        self.path_log = os.path.join(self.root_path, 'Archivos', 'Log\\')
        self.path_sql = os.path.join(self.root_path, 'Archivos', 'Sql\\')
        self.path_bat = os.path.join(self.root_path, 'Bat\\')

        if not os.path.exists(self.path):
            self.filelist = []
        else:
            self.filelist = os.listdir(self.path)

        self.filestg = os.path.join(self.path_temp, 'Banking_TMP_STG.csv')

        self.down_path = os.path.join(self.root_path, 'DOWNLOADED_FILES\\')

        if not os.path.exists(self.down_path):
            os.makedirs(self.down_path)


def move_file_super(path, filename):
    df = pandas.read_excel(path + filename)
    tipo = (filename[3:filename.find('-BANCO-en-')])
    path_month = ''
    if tipo == "ESTADO":
        cols = [1, 2, 3, 16]
        year = str(df.iloc[7, 4])
        df.drop(df.columns[cols], axis=1, inplace=True)
        print(f'Año - {year}')
        bank = df.iloc[1, 0]
        # Concatena el año del archivo con las columnas de meses
        df[8:9] = (str(year) + '-' + df[8:9].astype(str))

        # Elimina filas innecesarias
        df = df.drop([0, 1, 2, 3, 4, 5, 6, 7])
        # Se le da nombre a la columna de cuentas
        df.iloc[0, 0] = 'Cuentas'
        # Se declara la primera fila como el header del archivo
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header
        df_mes = df[1:2]
        df_mes1 = df_mes.loc[:, (df_mes != 0).any(axis=0)]
        print(df_mes1)
        mes = list(df_mes1.columns.values)[-1]
        if not os.path.exists(path + year):
            os.makedirs(path + year)
        if year == str(datetime.datetime.now().year):
            mes_actual = datetime.datetime.now()
            mes_actual = mes_actual.strftime("%B")
            print(mes_actual)
            if not os.path.exists(path + year + '/' + mes_actual):
                os.makedirs(path + year + '/' + mes_actual)
            path_month = ''.join([path, year, '/', mes_actual, '/'])
        else:
            path_month= "".join([path, year, '/'])
        if os.path.exists(path_month + filename):
            os.remove(path_month + filename)
        shutil.move(path + filename, path_month)
    if tipo == "BALANCE":
        cols = [3, 4, 5]
        df.drop(df.columns[cols], axis=1, inplace=True)
        year = str(df.iloc[7, 3])
        cols = [1, 2]
        df.drop(df.columns[cols], axis=1, inplace=True)
        df[8:9] = (str(year) + '-' + df[8:9].astype(str))
        # Elimina filas innecesarias
        df = df.drop([0, 1, 2, 3, 4, 5, 6, 7])
        # Se le da nombre a la columna de cuentas
        df.iloc[0, 0] = 'Cuentas'
        # Se declara la primera fila como el header del archivo
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header
        df_mes = df[1:2]
        df_mes1 = df_mes.loc[:, (df_mes != 0).any(axis=0)]
        mes = list(df_mes1.columns.values)[-1]
        if not os.path.exists(path + year):
            os.makedirs(path + year)
        if year == str(datetime.datetime.now().year):
            mes_actual = datetime.datetime.now()
            mes_actual = mes_actual.strftime("%B")
            print(mes_actual)
            if not os.path.exists(path + year + '/' + mes_actual):
                os.makedirs(path + year + '/' + mes_actual)
            path_month = path + year + '/' + mes_actual + '/'
        else:
            path_month = path + year + '/'
        if os.path.exists(path_month + filename):
            os.remove(path_month + filename)
        shutil.move(path + filename, path_month)


def clean_excel(path, filename):
    path = os.path.join(path, filename)
    df = pandas.read_excel("".join([path, filename]))
    tipo = (filename[3:filename.find('-BANCO-en-')])
    if tipo == "ESTADO":
        cols = [1, 2, 3,16]
        year = df.iloc[7, 4]
        df.drop(df.columns[cols], axis=1, inplace=True)
        # cols = [1, 2]
        Bank = df.iloc[1, 0]
        # df.drop(df.columns[cols], axis=1, inplace=True)
        # Concatena el año del archivo con las columnas de meses
        df[8:9] = (str(year) + '-' + df[8:9].astype(str))
        # Elimina filas innecesarias
        df = df.drop([0, 1, 2, 3, 4, 5, 6, 7])
        # Se le da nombre a la columna de cuentas
        df.iloc[0, 0] = 'Cuentas'
        # Se declara la primera fila como el header del archivo
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header
    if tipo == "BALANCE":
        cols = [3, 4, 5]
        df.drop(df.columns[cols], axis=1, inplace=True)
        year = df.iloc[7, 3]
        cols = [1, 2]
        df.drop(df.columns[cols], axis=1, inplace=True)
        # Concatena el año del archivo con las columnas de meses
        df[8:9] = (str(year) + '-' + df[8:9].astype(str))
        # Elimina filas innecesarias
        df = df.drop([0, 1, 2, 3, 4, 5, 6, 7])
        # Se le da nombre a la columna de cuentas
        df.iloc[0, 0] = 'Cuentas'
        # Se declara la primera fila como el header del archivo
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header

    return df

def df_year(self, path, filename, tipo):
    df = pandas.read_excel("".join([path, filename]))
    bank = df.iloc[1, 0]
    year = None
    if tipo == "ESTADO":
        cols = [1, 2, 3,16]
        year = df.iloc[7, 4]
    if tipo=='BALANCE':
        cols = [3, 4, 5]
        df.drop(df.columns[cols], axis=1, inplace=True)
        year = df.iloc[7, 3]
    args = [year, bank]
    return year, bank

def unpivot(self, df, tipo, year, fecha_subida, bank, bank_short_name):
    columns = list(df)
    # Crear dos listas para almacenar los headers y los meses
    headers = []
    months = []
    # Hacer un split entre las columnas a despivotear y las demas cabeceras
    df2 = None
    print(columns)
    for col in columns:
        if col.find('-') > 0:
            months.append(col)
        else:
            headers.append(col)

        df2 = pandas.melt(df,
                          id_vars=headers,
                          value_vars=months,
                          var_name='periodo',
                          value_name='Monto')

        df2['Fecha Creacion'] = (year + "01" + "01")
        df2['Fecha Subida'] = fecha_subida
        df2['Tipo'] = tipo
        df2['Bank'] = bank
        df2['Bank_shortname'] = bank_short_name
    return df2
