import sys
import os
import subprocess
import Classes as cl

def filecontrol(file,Company,fecha_creacion,fecha_subida,estado,error ):
    if len(sys.argv) > 7:
        def err(*args):
            global error
            error = ''
            for e in args:
                error = error + ' ' + e


        print(', '.join(sys.argv[5:]))
        err(', '.join(sys.argv[5:]))

    print(error)
    sql = cl.path_sql + "filecontrol.sql"

    if os.path.exists(sql):
        os.remove(sql)
    if estado == 'Succesfull' or estado == 'null':
        print("El estado del csv es: " + estado)
        # Borra el archivo sql temporal donde se reemplazan las variables.
        with open(cl.path_sql + "filecontrol.sql", "w") as f:
            # Reemplazamos las variables del archivo

            f.write(
                'update stg_filecontrol' + " set filename='" + file + "', company='" + Company + "' , fechacreacion='" + fecha_creacion + "', fechasubida='" + fecha_subida + "', estado='" + estado + "', errorcode=null where filename='" + file + "'  and company='" + Company + """';
    
                insert into stg_filecontrol  (filename, company, fechacreacion,fechasubida,estado,errorcode)""" + " Select '" + file + "','" + Company + "','" + fecha_creacion + "','" + fecha_subida + "','" + estado + "',null where NOT EXISTS" + ' (select 1 from stg_filecontrol' + " where filename='" + file + "');")
            f.close()

    if estado != 'Succesfull':
        print("El estado del csv es: " + estado)
        # Borra el archivo sql temporal donde se reemplazan las variables.
        with open(cl.path_temp + "filecontrol.sql", "w") as f:
            # Reemplazamos las variables del archivo
            f.write(
                'update stg_filecontrol' + " set filename='" + file + "', company='" + Company + "' , fechacreacion='" + fecha_creacion + "', fechasubida='" + fecha_subida + "', estado='" + estado + "', errorcode='" + error + "' where filename='" + file + "'  and company='" + Company + "' and fechacreacion='" + fecha_creacion + """';
    
                       insert into stg_filecontrol  (filename, company, fechacreacion,fechasubida,estado,errorcode)""" + " Select '" + file + "','" + Company + "','" + fecha_creacion + "','" + fecha_subida + "','" + estado + "', '" + error + "' where NOT EXISTS" + ' (select 1 from stg_filecontrol' + " where filename='" + file + "' and fechacreacion='" + fecha_creacion + "');")
            f.close()

    subprocess.call([r"'"+cl.path_bat+"filecontrol.bat'"])
    print("Terminado")